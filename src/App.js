import React from 'react';
import './App.scss';
import Navbar from './components/navbar/Navbar';
import Footer from './components/footer/Footer';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './components/pages/homePage/Home'
import Project from './components/pages/projects/Project';

function App() {
  return (
    <Router>
      <Navbar />
      <Switch>
        <Route path='/' exact component={Home}/>
        <Route path='/project' exact component={Project}/>
      </Switch>
      <Footer />
    </Router>
  );
}

export default App;
