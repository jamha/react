import React from 'react';
import {  FaGithubAlt, FaLinkedin } from 'react-icons/fa';
import {SiGitlab} from 'react-icons/si'
import './Footer.scss'

function Footer() { 
    return (
        <>
            <div className='footer'>
                <span>source code available&nbsp;<a href='https://gitlab.com/jamha/react' target='_blank'>here</a></span>
                <div class='footer-links'>
                    <a href='https://gitlab.com/jamha' target='_blank'><SiGitlab></SiGitlab></a>
                    <a href='https://github.com/james-dev-js' target='_blank'><FaGithubAlt></FaGithubAlt></a>
                    <a href='https://www.linkedin.com/in/james-hassall-a64744186/' target='_blank'><FaLinkedin></FaLinkedin></a>
                </div>
            </div>
        </>
    )
}

export default Footer