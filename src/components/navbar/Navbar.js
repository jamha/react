import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { AiFillBug } from "react-icons/ai";
import { FaBars, FaTimes } from 'react-icons/fa';
import './Navbar.scss'

function Navbar() {
    const [click, setClick] = useState(false);

    const handleClick = () => setClick(!click);
    const closeMobileMenu = () => setClick(false);

    return (
        <>
            <div className="navbar">
                <div className="navbar-container container">
                    <Link to='/' className="navbar-logo" onClick={closeMobileMenu}>
                    James Hassall
                    </Link>
                    <div className="menu-icon" onClick={handleClick}>
                        {click ? <FaTimes /> : <FaBars />}
                    </div>
                    <ul className={click ? 'nav-menu active' : 'nav-menu'}>
                        <li className="nav-item">
                            <Link to='/' className='nav-links' onClick={closeMobileMenu}>
                                Overview
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to='/project' className='nav-links' onClick={closeMobileMenu}>
                                Projects
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to='/experience' className='nav-links' onClick={closeMobileMenu}>
                                Experience
                            </Link>
                        </li>
                    </ul>
                </div>    
            </div>
        </>
    )
}

export default Navbar
