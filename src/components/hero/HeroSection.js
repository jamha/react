import React from 'react';
import { Button } from '../button/Button'
import { Link } from 'react-router-dom';
import './HeroSection.scss' 

function HeroSection({
    lightBg, topLine, lightText, lightTextDesc, headline, description, button, buttonLabel, buttonLink, img, alt, imgStart, table
}) {
    
    return (
        <>
            <div className={lightBg ? 'home__hero-section' : 'home__hero-section darkBg'}>

                <div className='container'>
                    <div className={imgStart ? 'row home__hero-row start' : 'row home__hero-row'}>

                        <div className={ img === '' ? 'col hero-text-only' : 'col'}>
                            <div className='home__hero-text-wrapper'>
                                <div className='top-line'>{topLine}</div>
                                <h1 className={lightText ? 'heading' : 'heading dark'}>{headline}</h1>
                                {
                                    description !== null && 
                                        <p className={lightTextDesc ? 'home__hero-subtitle' : 'home__hero-subtitle dark'}>{description}</p>
                                }
                                {
                                    table !== undefined &&
                                        <table className='hero-table'>
                                            <thead>
                                                <th></th>
                                                <th></th>
                                            </thead>
                                            <tbody>
                                                { table.map((row,index) => {
                                                    const {heading, description} = row
                                                    return (
                                                    <tr>
                                                        <td className='tr-heading'>{heading}</td>
                                                        <td className='tr-description'>{description}</td>
                                                    </tr>)
                                                })}
                                                
                                            </tbody>
                                        </table>
                                }
                                {
                                    button === true &&
                                    <Link to={buttonLink}>
                                        <Button buttonSize='btn--wide' buttonColor='secondary'>{buttonLabel}</Button>
                                    </Link>
                                }
                                
                            </div>
                        </div>
                    
                        <div className={ img === '' ? 'col hide' : 'col'}>
                            <div className='home__hero-img-wrapper'>
                                <img src={img} alt={alt} className='home__hero-img'></img>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </>
    )
}

export default HeroSection;
