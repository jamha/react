export const projObjOne = {
    lightBg: false,
    lightText: true,
    lightTextDesc: true,
    topLine: 'Website',
    headline: '',
    description: 'My website which I\'m always working on an constantly adding to.',
    button: false,
    buttonLabel: 'See Projects',
    imgStart: true,
    img: 'images/website.png',
    alt: 'My Website'
}

export const projObjTwo = {
    lightBg: false,
    lightText: true,
    lightTextDesc: true,
    topLine: 'Advent Of Code 2020',
    headline: '',
    description: 'A coding exercise which you can',
    button: true,
    buttonLabel: 'Explore More',
    buttonLink: '',
    imgStart: false,
    img: 'images/adventOfCode.png',
    alt: ''
}

