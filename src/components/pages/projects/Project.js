import './Project.scss';
import React from 'react';
import HeroSection from '../../hero/HeroSection';
import { projObjOne, projObjTwo } from './Data.js'

function Project() {
    return (
        <>
            <HeroSection {...projObjOne} />
            <HeroSection {...projObjTwo} />
        </>
    );
}

export default Project;