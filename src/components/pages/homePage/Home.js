import './Home.scss';
import React from 'react';
import HeroSection from '../../hero/HeroSection';
import { homeObjOne, homeObjTwo } from './Data.js'

function Home() {
    return (
        <>
            <HeroSection {...homeObjOne} />
            <HeroSection {...homeObjTwo} />
        </>
    );
}

export default Home;