export const homeObjOne = {
    lightBg: false,
    lightText: true,
    lightTextDesc: true,
    topLine: '',
    headline: 'Full-stack software developer currently based in Melbourne, interested in computing infrastructure, games developement and fin-tech projects. Always finding time to read up on the latest exploits ',
    description: '',
    button: false,
    buttonLabel: 'See Projects',
    imgStart: false,
    img: '',
    alt: ''
}

export const homeObjTwo = {
    lightBg: false,
    lightText: true,
    lightTextDesc: true,
    topLine: 'Skills',
    headline: '',
    description: '',
    table: [
        {heading: 'Languages', description: 'ASM, C#, C++, Java, JavaScript/TypeScript, Python,HTML, PHP, CSS'},
        {heading: 'Frameworks', description: 'Express.js, Apache, ReactJs, AngularJs, NodeJs'},
        {heading: 'Databases', description: 'MongoDB, SQL, SQLite, NoSQL'},
        {heading: 'Cloud', description: 'AWS: Ec2, S3, Route 53, SNS, SES'},
        {heading: 'Code Management', description: 'GitHub, GitBucket, GitLab'}
    ],
    button: false,
    buttonLabel: 'See Experience!',
    imgStart: false,
    img: '',
    alt: 'This is fine Dog'
}

